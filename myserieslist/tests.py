from django.test import TestCase


class DefaultTest(TestCase):
    def test_default_client(self):
        """
        Tests that / is reachable
        """
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)