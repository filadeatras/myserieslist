from django.conf.urls import patterns, include, url
from rest_framework import routers
from django.contrib import admin
admin.autodiscover()

# API
from shows import views
router = routers.DefaultRouter()
router.register(r'shows', views.ShowViewSet)

urlpatterns = patterns('',
                       url(r'^$', 'myserieslist.views.index', name='index'),
                       url(r'^styleguide/$', 'myserieslist.views.styleguide', name='styleguide'),

                       # Others
                       url('', include('social.apps.django_app.urls', namespace='social')),
                       url(r'^signup-email/', 'registration.views.signup_email'),
                       url(r'^email-sent/', 'registration.views.validation_sent'),
                       url(r'^login/$', 'registration.views.home'),
                       url(r'^logout/$', 'registration.views.logout'),
                       url(r'^done/$', 'registration.views.done', name='done'),
                       url(r'^email/$', 'registration.views.require_email', name='require_email'),


                       # Uncomment the admin/doc line below to enable admin documentation:
                       # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       # Uncomment the next line to enable the admin:
                       url(r'^admin/', include(admin.site.urls)),

                       # API
                       url(r'^api/', include(router.urls))
)