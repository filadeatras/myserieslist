from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

def index(request):
    return render_to_response('index.html', {}, context_instance=RequestContext(request))

def styleguide(request):
    return render_to_response('styleguide.html', {}, context_instance=RequestContext(request))
