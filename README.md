# MySeriesList

MySeriesList is a website that allows you to keep an up-to-date list of all the shows and movies that you have seen
and also see all those that you are missing out on.

## Architecture



## Installation

The project includes a `requirements.txt` file which you can install into your virtualenv by using `pip install -r requirements.txt`

```sh
git clone git@bitbucket.org:franhp/myserieslist.git
cd myserieslist
virtualenv myserieslist_env --distribute
. myserieslist_env/bin/activate
pip install -r requirements.txt
```

Then run it with:

```
foreman start
```

## Rules

* Test your code before you push the code
* Always write at least a test to prove that the code in your commit works
* Ensure that the code that you have written is [[ PEP8 | http://www.python.org/dev/peps/pep-0008/ ]] compilant
* Lines cannot be longer than 120 characters (80 preferred)
* [[ Git Flow | http://nvie.com/posts/a-successful-git-branching-model/]] will be used and a pull request will need to be requested whenever code needs to be merged to `develop`
* The comitter cannot approve its own pull request