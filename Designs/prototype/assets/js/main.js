(function() {
  $(function() {
    return $(window).scroll(function() {
      if ($(window).scrollTop() >= 260) {
        return $("#mobile-panel").show();
      } else {
        return $("#mobile-panel").hide();
      }
    });
  });

}).call(this);
