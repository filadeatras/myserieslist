# What Fran expects?

* (10) List all series (even animes) and movies with some details (synopsis, genre, actors, director, ...) and links to imdb, tvrage, rottentomatoes, animenewsnetwork, ...
    * (7)Ordering by rating
    * (7)Display number of people following it
    * (4)Filter by genre
* (10) User can mark any episode or movie as watched, want to see or skipped
* (9) User can make comments regarding any episode or movie and also rate them
* (9) User can decide to follow a tv series / anime and will be notified every time that there's a new episode
* (9) User will have a timeline (facebook style) where they can put what they are watching at the moment, which updates the status of the series if you are following it, otherwise it just puts it into the timeline
    * (8) You earn badges depending on the scenario. Ex: watched 50th Anniversary Doctor Who earns you a Doctor Who badge
    * (8) Users can have friends in the community of users and get notifications on what friends are doing
    * (7) Tweet, Facebook, Google plus those messages
    * (1) Plugins for Plex and vlc in the distant future, like lastfm for music players
* (5) Series will have a link to watch them online in the preferred language
* (4) Users can have movie watchlists
* (4) Users can sync to a calendar with the series they are following
* (2) Users can see when was the last time they saw a movie/show

# Las dudas de Héctor

* ¿Quién es nuestro target?
* ¿Cómo definimos el proyecto?
* ¿Qué tipo de licencia tendrá nuestro proyecto?
* ¿Será OpenSource?

# Las premisas de Héctor

* Marcarse objetivos alcanzables y realistas a corto, medio y largo plazo
* Zen de Python: http://www.python.org/dev/peps/pep-0020/
* Utilizar API's e información siempre de una forma legal
* En el código las cosas se hacen de dentro hacia afuera
* En el diseño las cosas se hacen de fuera hacia dentro
* Ante la duda la más tet...
* Si pesa más que un pollo me la fo...

# ¿Qué espera Héctor?

* Registro social +++
* Interfaz adaptativa, que funcione en cualquier dispositivo +++
* Disponibilidad de los capítulos en alguna web externa y legal si es posible +++
* Estadísticas de tus series +++
* Ranking de las mejores series, películas o animes +++
* Conseguir mucho tráfico, por tanto trabajar en indexación, buscadores, schemas, etc. +++
* Publicidad para mantener el proyecto (es la excusilla xD) +++
* Interfaz Multilenguaje pero por ahora datos sólo en inglés ++
* Calendario de tus series en 'viendo' ++
* Firma de tu perfil para vacilar de tus series ++
* En vez de spamear a los usuarios con mierda, crear subscripciones semanales para avisarles de diferentes cosas ++
* Preferiblemente comentarios vía terceros tipo disqus o intensedebate antes que implementarlos nativos ++
* A nivel de usuario personalizable, que se sienta que tu perfil te lo haces tu +
* Chat en tiempo real para comentar series on air +
* Aprender demasié +++