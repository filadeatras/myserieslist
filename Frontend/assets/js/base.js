/*
Sticky User Panel
Description: Display or hide the panel at 260px from top
*/


(function() {
  $(function() {
    return $(window).scroll(function() {
      if ($(window).scrollTop() >= 220) {
        return $("div#mobile-panel").show();
      } else {
        return $("div#mobile-panel").hide();
      }
    });
  });

  /*
  Show Left column 
  Description: Display or hide the left column in mobile
  */


  $(function() {
    return $('.show-menu').click(function() {
      if ($('div#left').position().left > 280) {
        $(window).scrollTop(220);
        return $('div#left').animate({
          'right': '0px'
        }, 500);
      } else {
        return $('div#left').animate({
          'right': '-320px'
        }, 500);
      }
    });
  });

}).call(this);
