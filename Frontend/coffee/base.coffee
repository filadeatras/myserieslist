###
Sticky User Panel
Description: Display or hide the panel at 260px from top
###

$ ->
	$(window).scroll -> 
    if ($(window).scrollTop() >= 220)
      $("div#mobile-panel").show();
    else
      return $("div#mobile-panel").hide(); 

###
Show Left column 
Description: Display or hide the left column in mobile
###

$ ->
  $('.show-menu').click ->
    #alert($('div#left').position().left)
    if ( $('div#left').position().left > 280)
      #$('body, html').animate({scrollTop:220}, '750')
      $(window).scrollTop(220) #no animation
      $('div#left').animate({'right':'0px'},500)
    else
      $('div#left').animate({'right':'-320px'},500);

    