#!/usr/bin/python
import json

from gevent.pywsgi import WSGIServer
import tvdb_api


def search_for(show_name):
    errors = []
    # Step 0: Check that it doesn't exist yet

    # Step 1: Try the local cache

    # Step 2: Try Elastic Search

    # Step 3: Try the local database

    # Step 4: Try the TVDB API
    try:
        t = tvdb_api.Tvdb(banners=True, search_all_languages=True)
        show = t[show_name]
    except Exception, err:
        errors.append(err)

    # Step 5: Try TheMovieDB API


    # Step 6: Try IMDB

    # Step 7:

    return {'name': show['seriesname'],
            'network': show['network']}


def application(env, start_response):
    json_content_type = [('Content-Type', 'application/json')]
    path = env['PATH_INFO']
    if path == '/search':
        # Expect a parameter named "name" that will contain whatever the user is searching
        request = eval(env['wsgi.input'].readlines()[0])
        try:
            show = search_for(request['name'])
            start_response('200 OK', json_content_type)
            return [json.dumps(show)]
        except Exception, err:
            start_response('404 Not Found', json_content_type)
            return [json.dumps(error="{'error':'%s'}" % err)]
    elif path == '/update_episodes':
        start_response('404 Not Found', json_content_type)
        return [json.dumps("{'update': 'done!'}")]
    else:
        start_response('404 Not Found', [('Content-Type', 'application/json')])
        return [json.dumps("{'error':'No endpoint specified'}")]


if __name__ == '__main__':
    print('Serving on 8080...')
    WSGIServer(('', 8080), application).serve_forever()


