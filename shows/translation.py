from modeltranslation.translator import translator, TranslationOptions
from shows.models import Show

class ShowTranslationOptions(TranslationOptions):
    fields = ('title',)

translator.register(Show, ShowTranslationOptions)