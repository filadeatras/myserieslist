from shows.models import Show
from rest_framework import viewsets
from shows.serializers import ShowSerializer
import logging

logger = logging.getLogger(__name__)

class ShowViewSet(viewsets.ModelViewSet):
    """
    API endpoint to allow users to see the shows
    """
    queryset = Show.objects.all()
    serializer_class = ShowSerializer